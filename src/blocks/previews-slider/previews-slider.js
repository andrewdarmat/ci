$('.previews-slider').slick({
  mobileFirst: true,
  arrows: false,
  dots: true,
  responsive: [
    {
      breakpoint: 767,
      settings: 'unslick'
    }
  ]
});
$('.previews-slider--full').slick({
  mobileFirst: true,
  arrows: false,
  dots: true,
  infinite: false,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 991,
      settings: 'unslick'
    }
  ]
});