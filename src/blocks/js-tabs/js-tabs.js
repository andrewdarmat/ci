$('.js-tabs__content').slick({
  infinite: false,
  fade: true,
  arrows: false,
  adaptiveHeight: true,
  draggable: false,
  swipe: false
});

function menuTabsScroll(){
  $('.js-tabs__menu-scroll').each(function(i,el){
    var menu = $(el);
    if($(window).width()<1230){
      if (!menu.hasClass('slick-initialized')) {
        menu.slick({
          variableWidth: true,
          infinite: false,
          arrows: false,
          focusOnSelect: true,
          swipeToSlide: true
        });
      }
    }
    else{
      if(menu.hasClass('slick-initialized')){
        menu.slick('unslick');
      }
    }

  });
}
menuTabsScroll();
$(window).resize(function(){
  menuTabsScroll();
});

$('.js-tabs__btn').click(function(e){
  e.preventDefault();
  var btn = $(this),
    menu = btn.parents('.js-tabs__menu'),
    tabs = $(menu.data('tabs'));
  if(!btn.hasClass('js-tabs__btn--active')){
    menu.find('.js-tabs__btn')
      .removeClass('js-tabs__btn--active tabs-primary__btn--active tabs-secondary__btn--active');
    if(btn.hasClass('tabs-primary__btn')){
      btn.addClass('js-tabs__btn--active tabs-primary__btn--active');
    }
    if(btn.hasClass('tabs-secondary__btn')){
      btn.addClass('js-tabs__btn--active tabs-secondary__btn--active');
    }
    tabs.slick('slickGoTo',btn.data('tab'));
    $('.js-tabs__menu-select[data-tabs="'+menu.data('tabs')+'"]').val(btn.data('tab'))
      .trigger('change');
  }
});

$('.js-tabs__menu-select').change(function(e){
  var select = $(this),
    tabs = select.data('tabs');
  $('.js-tabs__menu[data-tabs="'+tabs+'"] ' +
    '.js-tabs__btn[data-tab="'+select.val()+'"]').click();
});




if ($('.js-tabs').length) {
  var pageURL = window.location.href;
  var pagePathName = location.pathname;

  if(pageURL.indexOf(pagePathName) > -1) {
    var number = parseInt(pageURL.split("#").pop()) - 1;
    if (number > 0) {
      $('.tabs-primary__btn').removeClass('tabs-primary__btn--active');
      $('.tabs-primary__btn').eq(number).addClass('tabs-primary__btn--active');
      $('.js-tabs__content').slick('slickGoTo', number);
    }
  }

}
