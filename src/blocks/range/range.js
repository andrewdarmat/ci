$(document).ready(function(){
  $('.js-range').each(function(i,el){
    var range = $(el);
    var input = $(range.data('input'));
    var min = Number(range.data('min'));
    var max = Number(range.data('max'));
    input.val(range.data('from'));
    range.ionRangeSlider({
      hide_min_max: true,
      hide_from_to: true,
      onChange: function(data){
        input.val(data.from);
      },
      onUpdate: function(data){
        input.val(data.from);
      }
    });
    var rangeData = range.data("ionRangeSlider");
    input.change(function(){
      var  value = Number(input.val());
      switch (value) {
        case (value > max):
          value = max;
          break;
        case (value < min):
          value = min;
          break;
        default:
          break;
      }
      rangeData.update({
        from: value
      });
    });
  });
});