function checkPosition() {
    var width = $(window).width();
    if( width <= 992) {
        $('.tariffs-list').not('.slick-initialized').slick({
            mobileFirst:true,
            arrows: false,
            dots: false,
            centerMode: true,
            centerPadding: '20px',
            slidesToShow: 1
        });
    } else {
        $('.tariffs-list.slick-initialized').slick('unslick');
    }
}

$(document).ready(function() {
    checkPosition();
    $(window).resize(checkPosition);
});