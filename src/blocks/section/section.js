$('.section__article-more-btn').click(function(e) {
  e.preventDefault();
  $(this).hide();
  $(this).closest('.section__article').find('.section__article-more').fadeIn('fast');

  $('.js-tabs__content').find(".slick-slide").height("auto");
  $('.js-tabs__content').slick("setOption", '', '', true);
});
