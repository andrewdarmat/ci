$(".custom-control-input").click(function () {
  var checkboxID = $(this).prop('id');
  var checkboxNum = Number(checkboxID.replace(/\D+/g,""));
  var inputRange = "#calcRange" + checkboxNum;
  var input = $("#calc-input-" + checkboxNum);
  $(this).on("change", function () {
    if ($(this).prop('checked')){
      $(inputRange).prev().removeClass('irs--disabled');
      input.prop('disabled',false);
    }
    else{
      $(inputRange).prev().addClass('irs--disabled');
      input.prop('disabled',true);
    }
  });
})