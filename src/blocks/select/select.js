
$(document).ready(function() {
    $(".select").select2({
        width: 'resolve',
        minimumResultsForSearch: -1
    }).on("select2:open", function () {
        var ps = new PerfectScrollbar('.select2-results__options',{
            wheelSpeed: 200,
            suppressScrollX: true
        });  
        $(this).prev().css( "color", "#179e6e" );
    }).on("select2:close", function () {
        $(this).prev().css( "color", "#7d899c" );
    });
});

