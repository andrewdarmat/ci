function tooltips() {
  var $browserWidth = window.innerWidth || document.documentElement.clientWidth;
  if (($browserWidth < 992)) {
    $('.tooltip').on('click touchstart', function () {
      $(this).find('.tooltip__content').fadeIn(300);
    });
  } else {
    $('.tooltip').on('mouseenter', function () {
      $(this).find('.tooltip__content').fadeIn(300);
    });
    $('.tooltip').on('mouseleave', function () {
      $('.tooltip__content').fadeOut(300);
    });
  }
}

tooltips();

$(document).on('click touchstart', function(e) {
  var container = $('.tooltip');
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    $('.tooltip__content').fadeOut(300);
  }
});
