$('.stack__header-link').on('click', function (e) {
  e.preventDefault();
  $(this).toggleClass('stack__header-link--opened');
  $(this).parent().parent().parent().parent().find('.stack__content').toggle();
  $('.js-tabs__content').slick('reinit');
});