$('.header__toggle-btn').on('click', function (e) {
  e.preventDefault();
  $(this).toggleClass('active');
});

//function progress() {
//  var windowScrollTop = $(window).scrollTop();
//  var docHeight = $(document).height();
//  var windowHeight = $(window).height();
//  var progress = (windowScrollTop / (docHeight - windowHeight)) * 100;
//  var $bgColor = progress > 99 ? '#179e6e' : '#179e6e';
//
//  $('.header__bar').width(progress + '%').css({ backgroundColor: $bgColor });
//}
//progress();
//$(document).on('scroll', progress);

$('.header__toggle-btn').on('click', function (e) {
  e.preventDefault();
  $('.js-mobile-menu-bar').fadeToggle(300);
  $('.floating-buttons').toggleClass('floating-buttons--disabled');
});

var lastScrollTop = 0;
$(window).scroll(function(e) {
  var st = $(this).scrollTop();
  if (st > lastScrollTop) {
    $('.js-floating-buttons').removeClass('floating-buttons--active');
  } else {
    $('.js-floating-buttons').addClass('floating-buttons--active');
  }
  lastScrollTop = st;
});


function mobileMenu() {
  var $browserWidth = window.innerWidth || document.documentElement.clientWidth;
  if (($browserWidth <= 991)) {
    $('.menu__link').on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $('.menu__submenu').toggle();
    })
  }
}
mobileMenu();
$(window).on('resize', mobileMenu);
