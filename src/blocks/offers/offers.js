$(document).ready(function($) {
  $('.js-offers-equal-height-block').each(function(i,el){
    $(el).find('.js-offers-equal-height').each(function(i,el){
      $(el).attr('data-cell',i);
    });
  });
  $('.js-offers-equal-height-block').each(function(i,el){
    $(el).find('.js-offers-equal-height').each(function(i,el){
      var cell = $(el).data('cell');
      $('.js-offers-equal-height[data-cell='+ cell +']').equalHeightResponsive();
    });
  });
});