$(document).ready(function(){
  $('.js-circle-progress').circleProgress({
    startAngle: -Math.PI/2,
    size: 110,
    fill: '#179E6E',
    thickness: 12,
    lineCap: 'round',
    emptyFill: 'rgba(125, 137, 156, 0.15)'
  });
});
$(document).on('click','.js-inquiry-open',function(e){
  e.preventDefault();
  $(this).toggleClass('is-open').parents('.js-inquiry').toggleClass('is-open');
});