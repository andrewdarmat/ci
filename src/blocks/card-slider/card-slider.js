function checkCardSlider() {
    var width = $(window).width();
    if( width <= 767 ) {
        $('.card-slider').not('.slick-initialized').slick({
            mobileFirst:true,
            arrows: false,
            slidesToShow: 1,
            dots: true,
            adaptiveHeight: true
        });
    } else {
        $('.card-slider.slick-initialized').slick('unslick');
    }
}

$(document).ready(function() {
    checkCardSlider();
    $(window).resize(checkCardSlider);
});