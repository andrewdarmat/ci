var optionsSly = {
  horizontal: 1,
  itemNav: 'basic',
  scrollBy: 0,
  speed: 300,
  mouseDragging: 1,
  touchDragging: 1,
  releaseSwing: true
};

if ($('#history-scroll').length) {
  var historySly = new Sly('#history-scroll', optionsSly).init();
  $(window).resize(function(){
    historySly.reload();
  });
}
