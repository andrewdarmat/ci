$('.field__input').on('focus active',function(){
  $(this).prev().addClass('field__label--focused');
});

$('.field__input').on('blur',function(){
  $(this).prev().removeClass('field__label--focused');
  if($(this).is(':valid')) {
    $(this).removeClass('field__input--error');
    $(this).prev().addClass('field__label--active ').removeClass('field__label--error');
    $(this).next().removeClass('field__hint--active');
  } else {
    $(this).addClass('field__input--error');
    $(this).prev().addClass('field__label--error');
    $(this).next().addClass('field__hint--active');
  }
});

$('.field__unveil').on('click', function(e) {
  e.preventDefault();
  var pwdType = $(this).parent().find('.field--password').attr('type');
  var newType = (pwdType === 'password') ? 'text' : 'password';
  $(this).parent().find('.field--password').attr('type', newType)
});

$('.field__input--phone').inputmask("+7 (999) 999-99-99");

if ($(".field__calendar")[0]){
  $(function() {
    $('.field__calendar').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd/mm/yy",
      numberOfMonths: 1,
      firstDay: 1
    });
  });
}
