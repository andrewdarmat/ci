if ($('.data-slider').length) {
  function dataSlider() {
    var $browserWidth = window.innerWidth || document.documentElement.clientWidth;
    if (($browserWidth <= 767)) {
      $('.data-slider').slick({
        dots: true,
        arrows: false,
      });
    }
  }
  dataSlider();
}