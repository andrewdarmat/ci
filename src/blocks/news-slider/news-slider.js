$('.news-slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  infinite: true,
  mobileFirst: false,
  variableWidth: true,
  prevArrow: '<button type="button" class="news-slider__prev"></button>',
  nextArrow: '<button type="button" class="news-slider__next"></button>',
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        variableWidth: false,
      }
    },
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        dots: true,
        variableWidth: false,
        slidesToShow: 1
      }
    }
  ]
});

