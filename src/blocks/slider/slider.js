if ($('.slider').length) {
  $('.slider').slick({
    dots: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          arrows: false
        }
      }
    ]
  });
}