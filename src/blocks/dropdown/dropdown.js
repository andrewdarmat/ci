$('.js-btn-question').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('dropdown__trigger--active');
  $('.js-dropdown-callback').toggle();
  $('.header').toggleClass('header--dropdown');
});

$('.dropdown__close').click(function(e) {
  e.preventDefault();
  $(this).closest('.dropdown').fadeOut('fast');
  $('.header').removeClass('header--dropdown');
})
