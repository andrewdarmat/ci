$('.comments-slider').slick({
  variableWidth: true,
  arrows: false,
  swipeToSlide: true,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        variableWidth: false,
//        swipeToSlide: false,
        slidesToShow: 1
      }
    },
    {
      breakpoint: 992,
      settings: {
        variableWidth: false,
        swipeToSlide: true,
        slidesToShow: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        variableWidth: false,
        swipeToSlide: false,
        slidesToShow: 1
      }
    }
  ]
});
