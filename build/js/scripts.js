





$(".custom-control-input").click(function () {
  var checkboxID = $(this).prop('id');
  var checkboxNum = Number(checkboxID.replace(/\D+/g,""));
  var inputRange = "#calcRange" + checkboxNum;
  var input = $("#calc-input-" + checkboxNum);
  $(this).on("change", function () {
    if ($(this).prop('checked')){
      $(inputRange).prev().removeClass('irs--disabled');
      input.prop('disabled',false);
    }
    else{
      $(inputRange).prev().addClass('irs--disabled');
      input.prop('disabled',true);
    }
  });
})

function checkCardSlider() {
    var width = $(window).width();
    if( width <= 767 ) {
        $('.card-slider').not('.slick-initialized').slick({
            mobileFirst:true,
            arrows: false,
            slidesToShow: 1,
            dots: true,
            adaptiveHeight: true
        });
    } else {
        $('.card-slider.slick-initialized').slick('unslick');
    }
}

$(document).ready(function() {
    checkCardSlider();
    $(window).resize(checkCardSlider);
});


$('.comments-slider').slick({
  variableWidth: true,
  arrows: false,
  swipeToSlide: true,
  focusOnSelect: true,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        variableWidth: false,
//        swipeToSlide: false,
        slidesToShow: 1
      }
    },
    {
      breakpoint: 992,
      settings: {
        variableWidth: false,
        swipeToSlide: true,
        slidesToShow: 1
      }
    },
    {
      breakpoint: 768,
      settings: {
        variableWidth: false,
        swipeToSlide: false,
        slidesToShow: 1
      }
    }
  ]
});



if ($('.data-slider').length) {
  function dataSlider() {
    var $browserWidth = window.innerWidth || document.documentElement.clientWidth;
    if (($browserWidth <= 767)) {
      $('.data-slider').slick({
        dots: true,
        arrows: false,
      });
    }
  }
  dataSlider();
}

$('.js-btn-question').click(function(e) {
  e.preventDefault();
  $(this).toggleClass('dropdown__trigger--active');
  $('.js-dropdown-callback').toggle();
  $('.header').toggleClass('header--dropdown');
});

$('.dropdown__close').click(function(e) {
  e.preventDefault();
  $(this).closest('.dropdown').fadeOut('fast');
  $('.header').removeClass('header--dropdown');
})



$('.field__input').on('focus active',function(){
  $(this).prev().addClass('field__label--focused');
});

$('.field__input').on('blur',function(){
  $(this).prev().removeClass('field__label--focused');
  if($(this).is(':valid')) {
    $(this).removeClass('field__input--error');
    $(this).prev().addClass('field__label--active ').removeClass('field__label--error');
    $(this).next().removeClass('field__hint--active');
  } else {
    $(this).addClass('field__input--error');
    $(this).prev().addClass('field__label--error');
    $(this).next().addClass('field__hint--active');
  }
});

$('.field__unveil').on('click', function(e) {
  e.preventDefault();
  var pwdType = $(this).parent().find('.field--password').attr('type');
  var newType = (pwdType === 'password') ? 'text' : 'password';
  $(this).parent().find('.field--password').attr('type', newType)
});

$('.field__input--phone').inputmask("+7 (999) 999-99-99");

if ($(".field__calendar")[0]){
  $(function() {
    $('.field__calendar').datepicker({
      yearRange: "-100:+3",
      defaultDate: null,
      changeMonth: true,
      changeYear: true,
      dateFormat: "dd/mm/yy",
      numberOfMonths: 1,
      firstDay: 1
    });
  });
}





$('.header__toggle-btn').on('click', function (e) {
  e.preventDefault();
  $(this).toggleClass('active');
});

//function progress() {
//  var windowScrollTop = $(window).scrollTop();
//  var docHeight = $(document).height();
//  var windowHeight = $(window).height();
//  var progress = (windowScrollTop / (docHeight - windowHeight)) * 100;
//  var $bgColor = progress > 99 ? '#179e6e' : '#179e6e';
//
//  $('.header__bar').width(progress + '%').css({ backgroundColor: $bgColor });
//}
//progress();
//$(document).on('scroll', progress);

$('.header__toggle-btn').on('click', function (e) {
  e.preventDefault();
  $('.js-mobile-menu-bar').fadeToggle(300);
  $('.floating-buttons').toggleClass('floating-buttons--disabled');
});

var lastScrollTop = 0;
$(window).scroll(function(e) {
  var st = $(this).scrollTop();
  if (st > lastScrollTop) {
    $('.js-floating-buttons').removeClass('floating-buttons--active');
  } else {
    $('.js-floating-buttons').addClass('floating-buttons--active');
  }
  lastScrollTop = st;
});


function mobileMenu() {
  var $browserWidth = window.innerWidth || document.documentElement.clientWidth;
  if (($browserWidth <= 991)) {
    $('.menu__link').on('click', function (e) {
      e.preventDefault();
      $(this).toggleClass('active');
      $('.menu__submenu').toggle();
    })
  }
}
mobileMenu();
$(window).on('resize', mobileMenu);


var optionsSly = {
  horizontal: 1,
  itemNav: 'basic',
  scrollBy: 0,
  speed: 300,
  mouseDragging: 1,
  touchDragging: 1,
  releaseSwing: true
};

if ($('#history-scroll').length) {
  var historySly = new Sly('#history-scroll', optionsSly).init();
  $(window).resize(function(){
    historySly.reload();
  });
}


if ($('.info-slider__init').length) {
  $('.info-slider__init').slick({
    dots: true,
    arrows: false,
  });
}
$(document).ready(function(){
  $('.js-circle-progress').circleProgress({
    startAngle: -Math.PI/2,
    size: 110,
    fill: '#179E6E',
    thickness: 12,
    lineCap: 'round',
    emptyFill: 'rgba(125, 137, 156, 0.15)'
  });
});
$(document).on('click','.js-inquiry-open',function(e){
  e.preventDefault();
  $(this).toggleClass('is-open').parents('.js-inquiry').toggleClass('is-open');
});


$('.js-tabs__content').slick({
  infinite: false,
  fade: true,
  arrows: false,
  adaptiveHeight: true,
  draggable: false,
  swipe: false
});

function menuTabsScroll(){
  $('.js-tabs__menu-scroll').each(function(i,el){
    var menu = $(el);
    if($(window).width()<1230){
      if (!menu.hasClass('slick-initialized')) {
        menu.slick({
          variableWidth: true,
          infinite: false,
          arrows: false,
          focusOnSelect: true,
          swipeToSlide: true
        });
      }
    }
    else{
      if(menu.hasClass('slick-initialized')){
        menu.slick('unslick');
      }
    }

  });
}
menuTabsScroll();
$(window).resize(function(){
  menuTabsScroll();
});

$('.js-tabs__btn').click(function(e){
  e.preventDefault();
  var btn = $(this),
    menu = btn.parents('.js-tabs__menu'),
    tabs = $(menu.data('tabs'));
  if(!btn.hasClass('js-tabs__btn--active')){
    menu.find('.js-tabs__btn')
      .removeClass('js-tabs__btn--active tabs-primary__btn--active tabs-secondary__btn--active');
    if(btn.hasClass('tabs-primary__btn')){
      btn.addClass('js-tabs__btn--active tabs-primary__btn--active');
    }
    if(btn.hasClass('tabs-secondary__btn')){
      btn.addClass('js-tabs__btn--active tabs-secondary__btn--active');
    }
    tabs.slick('slickGoTo',btn.data('tab'));
    $('.js-tabs__menu-select[data-tabs="'+menu.data('tabs')+'"]').val(btn.data('tab'))
      .trigger('change');
  }
});

$('.js-tabs__menu-select').change(function(e){
  var select = $(this),
    tabs = select.data('tabs');
  $('.js-tabs__menu[data-tabs="'+tabs+'"] ' +
    '.js-tabs__btn[data-tab="'+select.val()+'"]').click();
});




if ($('.js-tabs').length) {
  var pageURL = window.location.href;
  var pagePathName = location.pathname;

  if(pageURL.indexOf(pagePathName) > -1) {
    var number = parseInt(pageURL.split("#").pop()) - 1;
    if (number > 0) {
      $('.tabs-primary__btn').removeClass('tabs-primary__btn--active');
      $('.tabs-primary__btn').eq(number).addClass('tabs-primary__btn--active');
      $('.js-tabs__content').slick('slickGoTo', number);
    }
  }

}

$('.leaders-slider').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    infinite: true,
    mobileFirst: false,
    variableWidth: true,
    prevArrow: '<button type="button" class="leaders-slider__prev"></button>',
    nextArrow: '<button type="button" class="leaders-slider__next"></button>',
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                variableWidth: false,
            }
        },
        {
            breakpoint: 768,
            settings: {
                arrows: false,
                variableWidth: false,
                slidesToShow: 1,
                dots: true
            }
        }
    ]
  });
  




$('.modal__close').click(function(e) {
  e.preventDefault();
  $(this).closest('.modal__wrapper').removeClass('modal__wrapper--active');
});


$('.news-slider').slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  infinite: true,
  mobileFirst: false,
  variableWidth: true,
  prevArrow: '<button type="button" class="news-slider__prev"></button>',
  nextArrow: '<button type="button" class="news-slider__next"></button>',
  responsive: [
    {
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        variableWidth: false,
      }
    },
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        dots: true,
        variableWidth: false,
        slidesToShow: 1
      }
    }
  ]
});



$(document).ready(function($) {
  $('.js-offers-equal-height-block').each(function(i,el){
    $(el).find('.js-offers-equal-height').each(function(i,el){
      $(el).attr('data-cell',i);
    });
  });
  $('.js-offers-equal-height-block').each(function(i,el){
    $(el).find('.js-offers-equal-height').each(function(i,el){
      var cell = $(el).data('cell');
      $('.js-offers-equal-height[data-cell='+ cell +']').equalHeightResponsive();
    });
  });
});


$('.papers__count').click(function() {
    $(this).toggleClass('papers__count--open');
    $(this).closest('.papers').find('.papers__content').toggleClass('papers__content--open');
    $('.js-tabs__content').slick('reinit');
  });



$('.previews-slider').slick({
  mobileFirst: true,
  arrows: false,
  dots: true,
  responsive: [
    {
      breakpoint: 767,
      settings: 'unslick'
    }
  ]
});
$('.previews-slider--full').slick({
  mobileFirst: true,
  arrows: false,
  dots: true,
  infinite: false,
  responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2
      }
    },
    {
      breakpoint: 991,
      settings: 'unslick'
    }
  ]
});
$(document).ready(function(){
  $('.js-range').each(function(i,el){
    var range = $(el);
    var input = $(range.data('input'));
    var min = Number(range.data('min'));
    var max = Number(range.data('max'));
    input.val(range.data('from'));
    range.ionRangeSlider({
      hide_min_max: true,
      hide_from_to: true,
      onChange: function(data){
        input.val(data.from);
      },
      onUpdate: function(data){
        input.val(data.from);
      }
    });
    var rangeData = range.data("ionRangeSlider");
    input.change(function(){
      var  value = Number(input.val());
      switch (value) {
        case (value > max):
          value = max;
          break;
        case (value < min):
          value = min;
          break;
        default:
          break;
      }
      rangeData.update({
        from: value
      });
    });
  });
});



$('.section__article-more-btn').click(function(e) {
  e.preventDefault();
  $(this).hide();
  $(this).closest('.section__article').find('.section__article-more').fadeIn('fast');

  $('.js-tabs__content').find(".slick-slide").height("auto");
  $('.js-tabs__content').slick("setOption", '', '', true);
});


$(document).ready(function() {
    $(".select").select2({
        width: 'resolve',
        minimumResultsForSearch: -1
    }).on("select2:open", function () {
        var ps = new PerfectScrollbar('.select2-results__options',{
            wheelSpeed: 200,
            suppressScrollX: true
        });  
        $(this).prev().css( "color", "#179e6e" );
    }).on("select2:close", function () {
        $(this).prev().css( "color", "#7d899c" );
    });
});


if ($('.slider').length) {
  $('.slider').slick({
    dots: true,
    arrows: true,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          arrows: false
        }
      }
    ]
  });
}

$('.stack__header-link').on('click', function (e) {
  e.preventDefault();
  $(this).toggleClass('stack__header-link--opened');
  $(this).parent().parent().parent().parent().find('.stack__content').toggle();
  $('.js-tabs__content').slick('reinit');
});






function checkPosition() {
    var width = $(window).width();
    if( width <= 992) {
        $('.tariffs-list').not('.slick-initialized').slick({
            mobileFirst:true,
            arrows: false,
            dots: false,
            centerMode: true,
            centerPadding: '20px',
            slidesToShow: 1
        });
    } else {
        $('.tariffs-list.slick-initialized').slick('unslick');
    }
}

$(document).ready(function() {
    checkPosition();
    $(window).resize(checkPosition);
});
function tooltips() {
  var $browserWidth = window.innerWidth || document.documentElement.clientWidth;
  if (($browserWidth < 992)) {
    $('.tooltip').on('click touchstart', function () {
      $(this).find('.tooltip__content').fadeIn(300);
    });
  } else {
    $('.tooltip').on('mouseenter', function () {
      $(this).find('.tooltip__content').fadeIn(300);
    });
    $('.tooltip').on('mouseleave', function () {
      $('.tooltip__content').fadeOut(300);
    });
  }
}

tooltips();

$(document).on('click touchstart', function(e) {
  var container = $('.tooltip');
  if (!container.is(e.target) && container.has(e.target).length === 0) {
    $('.tooltip__content').fadeOut(300);
  }
});





